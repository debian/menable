Linux Driver Changelog
======================

COMPATIBILITY NOTICE FOR LINUX KERNELS 4.5 OR NEWER:
The Linux Kernel starting with version 4.5 allows a stricter management of
PCI resources via setting CONFIG_IO_STRICT_DEVMEM.
The current Silicon Software Runtime will not work when this setting is
enabled. This is a known issue on some Linux distributions. To work-around
this issue, please boot the linux kernel with option iomem=relaxed, or
recompile the kernel with CONFIG_IO_STRICT_DEVMEM=n.

COMPATIBILITY NOTICE FOR UEFI SECURE BOOT:
If you install Linux on an UEFI machine using the Secure Boot option enabled
in the UEFI settings, drivers might need to be signed. Currently, the
Silicon Software Linux driver does not support signing during the build
process. Signing requires for the user to create a certificate, sign the
driver using the certificate and install the certificate into the UEFI
certificate store. There are several how-tos to be found online, if you
want to sign the driver yourself. The easiest work-around however is to
disable the UEFI Secure Boot feature in your mainboard settings.

NOTICE ON INSTALL-TIME ERROR MESSAGES:
When you install the driver on modern Linux distributions you may ancounter
error messages referring to driver signing. You can safely ignore those
error messages if the UEFI Secure Boot feature is disabled in your mainboard
settings.

Version 4.2.6
-------------
- Revision 73485
- BUG 8373: Fixed Allocation of UIQs on VCLx (Events were not working correctly)
- Fixed handling of negative Image numbers
- Fixed overflow handling for 32-bit image numbers
- BUG 8277: Fixed lock-up of image acquisition with more than one DMA when using socket API

Version 4.2.5
-------------
- Revision 72779
- Added support for high resolution timers
- BUG 6446: When buffers were over-allocated, the early termination of the DMA transfer could lead to corrupted images; fixed
- Use killable waits instead of interruptible to prevent non-fatal signals from interrupting (ctrl-z, forking, etc.)
- DMA timeout set via FG_TIMEOUT was limited to (INT_MAX-1)/1000 seconds (~25 days); fixed
- The maximum value for FG_TIMEOUT, 2147483646, is used for no DMA timeout
- Writing to mapped DMA buffers while the process uses fork() could lead to permanent damage to DMA transfers; fixed
- Sometimes adding memory buffers would fail with FG_INTERNAL_ERROR; fixed
- Added marathon VCLx

Version 4.2.4
-------------
- Revision 67383
- Added support for Linux Kernel Version 4.11
- Fixed allocation of UIQs on iron man CL
- Fixed error while stopping of DMA engine

Version 4.2.3
-------------
- Revision 65505
- Fixed allocation of UIQs on medium configuration

Version 4.2.2
-------------
- Revision 63993
- Added support for Linux Kernel Version 4.6 -> 4.10

Version 4.2.1
-------------
- Revision 62256
- BUG: Problems occur with high load of SPI Communcation. FIX: MSI Support disabled
- Spinlock problem fixed

Version 4.2.0
-------------
- Revision 61397
- Red hat linux support
- Added Support for Me5 Reconfiguration

Version 4.1.8
-------------
- Revision 60999
- Added MSI Support

Version 4.1.7
-------------
- Revision 60770
- Added Support for Abacus 4G Base
- Added Support for Abacus 4G Base II

Version 4.1.6
-------------
- Revision 58981
- Cleanup for DMA resources upon Process end

Version 4.1.5
-------------
- Revision 58280
- Added support for Linux Kernel Version >= 4.1

Version 4.1.4
-------------
- Revision 57304
- Added support for Linux Kernel Version < 3.13

Version 4.1.3
-------------
- Revision 57083
- Added Support for Marathon/LightBridge Boards
- Added Support for Async Notifications, LED control, POE Chip fault interrupt
- Printing Driver version

Version 4.1.0
-------------
- Revision 53908
- First Support for Me5
- Using single version number for the 32/64 bit drivers

