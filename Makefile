obj-m	+= menable.o

.PHONY: clean mrproper install archive

SOURCE_DIR = $(shell pwd)
BUILD_DIR = /lib/modules/$(shell uname -r)/build

INSTALL = /usr/bin/install
DEPMOD = /sbin/depmod

RHEL_MAJOR_VERSION = $(shell if [ -f /etc/redhat-release ]; then cat /etc/redhat-release | sed -r 's/.* release ([0-9]+).*/\1/'; else echo 0; fi)
ifeq ($(RHEL_MAJOR_VERSION),7)
	KBUILD_CFLAGS += -DRHEL7
endif

menable-y += fgrab.o
menable-y += linux_version.o
menable-y += menable4.o
menable-y += menable5.o
menable-y += menable_core.o
menable-y += menable_design.o
menable-y += menable_dma.o
menable-y += menable_ioctl.o
menable-y += menable_mem.o
menable-y += uiq.o

all: menable.ko

install: menable.ko udev/10-siso.rules udev/men_path_id udev/men_uiq udev/men_dma
	$(MAKE) -C $(BUILD_DIR) M=$(SOURCE_DIR) modules_install
	$(DEPMOD)
	$(INSTALL) udev/10-siso.rules /etc/udev/rules.d/
	$(INSTALL) udev/men_path_id udev/men_uiq udev/men_dma /sbin/

menable.ko: *.c *.h men_ioctl_codes.h sisoboards.h
	$(MAKE) -C $(BUILD_DIR) M=$(SOURCE_DIR) modules

archive: men_ioctl_codes.h sisoboards.h
	rm -f ../menable.tar.bz2
	tar cjf ../menable.tar.bz2 *

men_ioctl_codes.h:
	cp ../men_ioctl_codes.h .

sisoboards.h:
	cp ../../Common/SisoBoards/include/sisoboards.h .

clean:
	$(MAKE) -C $(BUILD_DIR) M=$(SOURCE_DIR) clean
	rm -f *~ *.bak *.o *.o.*

mrproper:
	$(MAKE) -C $(BUILD_DIR) M=$(SOURCE_DIR) clean
	rm -f men_ioctl_codes.h sisoboards.h *~ *.bak *.o *.o.*

