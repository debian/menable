/************************************************************************
 * Copyright 2006-2011 Silicon Software GmbH
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License (version 2) as
 * published by the Free Software Foundation.
 */
#ifndef MENABLE5_H
#define MENABLE5_H

#include <linux/sched.h>

#include "menable.h"

#define ME5_BOARDSTATUS     0             /**< Board status */
#define GETFIRMWAREVERSION(x) (((x) & 0xff00) | (((x) >> 16) & 0xff))
#define ME5_BOARDSTATUSEX   (0x001 * 4)   /**< Board ID */
#define ME5_BOARDTYPE_SHIFT 16            /**< Shift board status ex to get board type */
#define ME5_BOARDTYPE_MASK  0xffff        /**< Mask for board status ex after shift to get board type */

#define ME5_NUMDMA          (0x002 * 4)
#define ME5_IRQSTATUS       (0x100 * 4)
#define ME5_IRQACK          (0x102 * 4)
#define ME5_IRQTYPE         (0x104 * 4)
#define ME5_IRQENABLE       (0x106 * 4)

#define ME5_DMAOFFS         (0x110 * 4)
#define ME5_DMASZ           (0x010 * 4)

#define ME5_DMACTRL         (0x000 * 4)
#define ME5_DMAADDR         (0x002 * 4)
#define ME5_DMAACTIVE       (0x004 * 4)
#define ME5_DMALENGTH       (0x006 * 4)
#define ME5_DMACOUNT        (0x008 * 4)
#define ME5_DMAMAXLEN       (0x00a * 4)
#define ME5_DMATYPE         (0x00c * 4)
#define ME5_DMATAG          (0x00e * 4)

#define ME5_IRQQUEUE        (0x080 * 4)
#define ME5_IRQQ_LOW        16
#define ME5_IRQQ_HIGH       29

#define ME5_FULLOFFSET     (0x2000 * 4)
#define ME5_IFCONTROL      (0x0004 * 4)
#define ME5_UIQCNT         (0x0006 * 4)
#define ME5_FIRSTUIQ       (0x0007 * 4)
#define ME5_FPGACONTROL    (0x1010 * 4)
#define ME5_PCIECONFIG0    (0x0010 * 4)
#define ME5_PCIECONFIGMAX  (0x001f * 4)

#define ME5_LED_CONTROL                 (0x0012 * 4)
#define LED_W_CONTROL_ENABLE_MASK       0x1
#define LED_W_VALUE_MASK                0x1E
#define LED_R_REGISTER_VALID            0x1
#define LED_R_PRESENT_MASK              0x1E
#define LED_R_CONTROL_ENABLED_MASK      0x10000
#define LED_R_VALUE_MASK                0x1E0000

#define ME5_RECONFIGURE_CONTROL (0x0013 * 4)
#define RECONFIGURE_FLAG    0x1

#define ME5_DMA_FIFO_DEPTH  1LL

#define ME5_CLCAMERASTATUS      (0x0a00 * 4)  /**< CL status bits */
#define ME5_CLCHIPGROUPA_SHIFT  24      /**< Shift CL status to get chip group a configuration */
#define ME5_CLCHIPGROUPA_MASK   0x7     /**< Mask for CL status after shift to get chip group a configuration */
#define ME5_CLCHIPGROUPNOTUSED  0       /**< Chip group not used */
#define ME5_CLSINGLEBASE0       1       /**< Chip 0 used for single base configuration */
#define ME5_CLSINGLEBASE1       2       /**< Chip 1 used for single base configuration */
#define ME5_CLDUALBASE          3       /**< Chips 0 and 1 used for dual base configuration */
#define ME5_CLMEDIUM            4       /**< Chips 0 and 1 used for medium configuration */
#define ME5_CLFULL              5       /**< Chips 0..2 used for full configuration */

/* IRQ BIT MAPPING (FPGA IRQ Status/Acknowledgement/Enable registers) */
//DMA IRQ
#define INT_MASK_DMA_CHANNEL_IRQ            0x000000FF
//Alarms IRQ
#define INT_MASK_ALARMS                     0x0000BF00
// (Detailed Alarms IRQ)
#define INT_MASK_ACTION_CMD_LOST_CHAN_0     0x00000100
#define INT_MASK_ACTION_CMD_LOST_CHAN_1     0x00000200
#define INT_MASK_ACTION_CMD_LOST_CHAN_2     0x00000400
#define INT_MASK_ACTION_CMD_LOST_CHAN_3     0x00000800
#define INT_MASK_ACTION_CMD_LOST            0x00000F00
#define INT_MASK_PHY_MANAGEMENT             0x00001000
#define INT_MASK_POE                        0x00002000
#define INT_MASK_TEMPERATURE_ALARM          0x00008000
//User IRQ
#define INT_MASK_USER_QUEUE_IRQ             0x3FFF0000
//MSI disable flag (IRQ Enable register)
#define ME5_DISABLE_MSI_MASK                0x80000000ul

/* Notification sent from the driver (Software) */
#define NOTIFICATION_DRIVER_CLOSED  0x01    // Fired when driver closed the process
#define NOTIFICATION_DEVICE_REMOVED 0x02    // Fired when an existing device is removed (E.g. Thunderbolt device)
#define NOTIFICATION_DEVICE_ADDED   0x04    // Fired when a new device is added
#define NOTIFICATION_DEVICE_ALARM   0x08    // Fired when a notification IRQ is received

struct menable_uiq;

extern struct class *menable_notify_class;

struct me5_data {
    struct siso_menable * men;

    void *dummypage;
    dma_addr_t dummypage_dma;

    uint32_t design_crc;
    char design_name[65];                           /* user supplied name of design (for IPC) */

    // IRQ
    spinlock_t irqmask_lock;                        /* Protects IRQ Enable register (ME5_IRQENABLE) */
    uint32_t irq_wanted;                            /* 32-bits mapped to the IRQs that are of interest, protected by irqmask_lock */

    spinlock_t alarms_lock;                         /* Protects irq_wanted_alarms_status, notification/time_stamp variables */
    uint32_t irq_wanted_alarms_status;              /* Alarms of interest status (0 = off, 1 = fired) */
    // Notifications
    unsigned long notification;                     /* Current available notifications */
    unsigned long notification_time_stamp;          /* Current notifications time stamp */

    // Notifications Handlers
    spinlock_t notification_handler_headlock;       /* Protects notification_handler_heads list */
    struct list_head notification_handler_heads;    /* All buffer heads */

    // IRQ Work
    struct work_struct irq_work;                    /* Called directly after me5_irq is executed, to complete IRQ handling */

    // Temperature Alarm
    /*
     * Unlike other notifications, device over temperature requires long time to be fixed.
     * During this period, receiving repetitive interrupts are not required, therefore the alarm is
     * set off and reset every temperatureAlarmPeriod (default: 1000 ms).  temperature_alarm_work
     * is responsible for resetting the alarm.
     */
    struct delayed_work temperature_alarm_work;     /* called after temperature alarm is set (delayed for temperatureAlarmPeriod milliseconds) */
    unsigned int temperatureAlarmPeriod;            /* Minimum period between two consecutive temperature alarms (milliseconds) */

    // MSI
    struct work_struct msi_work;
};

static void me5_irq_ack (struct siso_menable *men, unsigned long alarm);

struct me5_notification_handler {
    struct list_head node;                      /* entry in notification_handler heads *_list */
    pid_t pid;                                  /* handler process id */
    pid_t ppid;                                 /* handler parent process id */
    unsigned long notification_time_stamp;      /* Time stamp of last handled notification */
    struct completion notification_available;   /* Notification Available Event: this is triggered whenever a new notification is available */
    bool quit_requested;                        /* true when quit is requested, false otherwise. */
};

#endif
